public class Main {
    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle("blue", 5.5f, 8.5f);
        System.out.println("RECTANGLE");
        System.out.println("The area of the rectangle is: " + rectangle.computeArea());
        System.out.println("The perimeter of the rectangle is: " + rectangle.computePerimeter());
        System.out.println("The color of the rectangle is: " + rectangle.getColour());

        Circle circle = new Circle("red", 7.5f);
        System.out.println("Circle");
        System.out.println("The area of the circle is: " + circle.computeArea());
        System.out.println("The perimeter of the circle is: " + circle.computePerimeter());
        System.out.println("The color of the circle is: " + circle.getColour());

    }
}
