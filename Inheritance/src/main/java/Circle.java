public class Circle extends Shape {

    private float radius;

    public Circle(String colour, float radius) {
        super(colour);
        this.radius = radius;
    }

    public double computeArea() {
        return Math.PI * (this.radius * this.radius);
    }

    public double computePerimeter() {
        return (2 * Math.PI) * this.radius;
    }
}
