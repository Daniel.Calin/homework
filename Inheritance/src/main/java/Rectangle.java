public class Rectangle extends Shape {

    private float width;
    private float length;

    public Rectangle(String colour, float width, float length) {
        super(colour);
        this.width = width;
        this.length = length;
    }

    public double computeArea() {
        return this.length * this.width;
    }

    public double computePerimeter() {
        return 2 * (this.width + this.length);
    }
}
