import java.util.InputMismatchException; // for input out of range for the expected type i.e. 0 or letter
import java.util.Scanner;

public class Game {
    static Scanner input;
    static String turn;

    public static void main(String[] args) {
        input = new Scanner(System.in);
        turn = "X";
        String winner = null;
        Board.populateEmptyBoard();

        System.out.println("Let's play!");
        System.out.println("--------------------------------");
        Board.printBoard();
        System.out.println("Enter a number from 1 to 9:");

        while (winner == null) {
            int numInput;
            try {
                numInput = input.nextInt();
                if (!(numInput > 0 && numInput <= 9)) {
                    System.out.println("Invalid input, number has to be from 1 to 9:");
                    continue;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input; re-enter index number:");
                continue;
            }
            if (Board.table[numInput - 1].equals(String.valueOf(numInput))) {
                Board.table[numInput - 1] = turn;
                if (turn.equals("X")) {
                    turn = "O";
                } else {
                    turn = "X";
                }
                Board.printBoard();
                winner = Board.checkWinner();
            } else {
                System.out.println("Slot already taken; enter another number:");
            }
        }
        if (winner.equalsIgnoreCase("draw")) {
            System.out.println("Draw");
        } else {
            System.out.println("Congratulations! " + winner + "'s have won!");
        }
    }
}
