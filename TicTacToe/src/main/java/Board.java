import java.util.Arrays;

public class Board {

    static String[] table = new String[9];

    static void printBoard() {
        System.out.println("/---|---|---\\");
        System.out.println("| " + table[0] + " | " + table[1] + " | " + table[2] + " |");
        System.out.println("|-----------|");
        System.out.println("| " + table[3] + " | " + table[4] + " | " + table[5] + " |");
        System.out.println("|-----------|");
        System.out.println("| " + table[6] + " | " + table[7] + " | " + table[8] + " |");
        System.out.println("/---|---|---\\");
    }

    static void populateEmptyBoard() {
        int i = 0;
        while (i < 9) {
            table[i] = String.valueOf(i + 1);
            i++;
        }
    }

    static String checkWinner() {
        for (int i = 0; i < 8; i++) {
            String line = null;
            switch (i) {
                case 0:
                    line = table[0] + table[1] + table[2];
                    break;
                case 1:
                    line = table[3] + table[4] + table[5];
                    break;
                case 2:
                    line = table[6] + table[7] + table[8];
                    break;
                case 3:
                    line = table[0] + table[3] + table[6];
                    break;
                case 4:
                    line = table[1] + table[4] + table[7];
                    break;
                case 5:
                    line = table[2] + table[5] + table[8];
                    break;
                case 6:
                    line = table[0] + table[4] + table[8];
                    break;
                case 7:
                    line = table[2] + table[4] + table[6];
                    break;
            }
            if (line.equals("XXX")) {
                return "X";
            } else if (line.equals("OOO")) {
                return "O";
            }
        }

        for (int i = 0; i < 9; i++) {
            if (Arrays.asList(table).contains(String.valueOf(i + 1))) {
                break;
            } else if (i == 8) return "draw";
        }

        System.out.println(Game.turn + "'s turn; enter a slot number:");
        return null;
    }
}