public class Address {
    private Country country;
    private String city;
    private String postcode;

    public Address() {

    }

    public Address(Country country, String city, String postcode) {
        this.country = country;
        this.city = city;
        this.postcode = postcode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
