public class Student extends Person {
    private College college;
    private int studyYear;

    public Student() {

    }

    public Student(College college, int studyYear, String name, String CNP, int age, Address address, int numberOfWorkedHours) {
        super(age, name, CNP, address, numberOfWorkedHours);
        this.college = college;
        this.studyYear = studyYear;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    public int getStudyYear() {
        return studyYear;
    }

    public void setStudyYear(int studyYear) {
        this.studyYear = studyYear;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
