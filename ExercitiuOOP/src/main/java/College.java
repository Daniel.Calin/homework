public class College {
    private String university;
    private String collegeName;
    private Country country;

    public College() {
    }

    public College(String university, String collegeName, Country country) {
        this.university = university;
        this.collegeName = collegeName;
        this.country = country;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
