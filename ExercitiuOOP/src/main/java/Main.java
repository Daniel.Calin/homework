public class Main {
    public static void main(String[] args) {
        Country studentCountry = new Country("England", "EN", true);
        Country employeeCountry = new Country("France", "FR", true);
        Country unemployedCountry = new Country("Norway", "NO", true);

        Country collegeCountry = new Country("United States of America", "US", false);
        Country companyCountry = new Country("United States of America", "US", false);

        Address studentAddress = new Address(studentCountry, "Birmingham", "KT2 7PY");
        Address employeeAddress = new Address(employeeCountry, "Lyon", "69998");
        Address unemployedAddress = new Address(unemployedCountry, "Bergen", "5814");

        Company company = new Company("NASA", companyCountry);
        College college = new College("Harvard ", "Kennedy School of Government", collegeCountry);

        Person personStudent = new Person(22, "Maria", "1980529330523", studentAddress, 0);
        Person personEmployee = new Person(34, "Daniel", "1860318330804", employeeAddress, 165);
        Person personUnemployed = new Person(29, "Victor", "1910212213294", unemployedAddress, 0);

        Student student = new Student(college, 2, personStudent.getName(), personStudent.getCNP(), personStudent.getAge(),
                personStudent.getAddress(), personStudent.getNumberOfWorkedHours());

        Employee employee = new Employee(company, 10000, personEmployee.getAge(), personEmployee.getCNP(), personEmployee.getName(),
                personEmployee.getAddress(), personEmployee.getNumberOfWorkedHours());

        Unemployed unemployed = new Unemployed(true, "Gamer", personUnemployed.getName(),
                personUnemployed.getCNP(), personUnemployed.getAge(), personUnemployed.getAddress(), personUnemployed.getNumberOfWorkedHours());

        System.out.println("Student data: " + "\n" + student.getName() + " is " + student.getAge() + "."
                + " Her CNP is: " + student.getCNP() + " and she is from " + studentCountry.getCountryName() + "("
                + studentCountry.getCountryCode() + ")" + " which is a EU country" + "(" + studentCountry.isEu() + ")."
                + "\nShe lives in " + studentAddress.getCity() + " and her postcode is " + studentAddress.getPostcode() + "."
                + "\nShe is a student at " + college.getUniversity() + college.getCollegeName() + " in the " + collegeCountry.getCountryName() + ".");
        System.out.println("Employee data: " + "\n" + personEmployee.getName() + " is " + employee.getAge() + "."
                + " His CNP is: " + personEmployee.getCNP() + " and he is from " + employeeCountry.getCountryName() + "("
                + employeeCountry.getCountryCode() + ")" + " which is a EU country" + "(" + employeeCountry.isEu() + ")."
                + "\nHe lives in " + employeeAddress.getCity() + " and his postcode is " + employeeAddress.getPostcode() + "."
                + "\nHe is working " + employee.getNumberOfWorkedHours() + " hours per month " + "at " + company.getCompanyName()
                + " in the " + companyCountry.getCountryName()
                + " earning " + employee.getSalary() + " $$$" + " per week");
        System.out.println("Unemployed data: " + "\n" + personUnemployed.getName() + " is " + unemployed.getAge() + "."
                + " His CNP is: " + personUnemployed.getCNP() + " and he is from " + unemployedCountry.getCountryName() + "("
                + unemployedCountry.getCountryCode() + ")" + " which is a EU country" + "(" + unemployedCountry.isEu() + ")."
                + "\nHe lives in " + unemployedAddress.getCity() + " and his postcode is " + unemployedAddress.getPostcode() + "."
                + " He is working " + "(" + unemployed.getNumberOfWorkedHoursInAWeek(unemployed.getNumberOfWorkedHours()) + ")"
                + " and he is a " + unemployed.getProfession() + ".");

    }
}
