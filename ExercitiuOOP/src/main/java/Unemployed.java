public class Unemployed extends Person {
    private boolean free;
    private String Profession;

    public Unemployed() {

    }

    public Unemployed(boolean free, String Profession, String name, String CNP, int age, Address address, int numberOfWorkedHours) {
        super(age, name, CNP, address, numberOfWorkedHours);
        this.free = free;
        this.Profession = Profession;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public String getProfession() {
        return Profession;
    }

    public void setProfession(String profession) {
        Profession = profession;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
